﻿Imports System
Imports System.IO
Imports System.Data

Public Module Command

#Region "Node Functions"
    ''' <summary>
    ''' ODBC Command
    ''' </summary>
    ''' <param name="ConnectionString">The connection string</param>
    ''' <param name="Toggle">Toggle the connection on/off</param>
    ''' <param name="Commands">The SQL Command</param>
    ''' <returns>Command Succes or Fail</returns>
    ''' <search>interoperability, database, slingshot, query</search>
    Public Function ODBC_Command(ConnectionString As String,
                                      Toggle As Boolean,
                                      Commands As List(Of String)) As Boolean

        Try
            Dim m_return As Boolean = False
            If Toggle = True Then
                Dim m_dbcmd As New clsRDBMS()

                m_return = m_dbcmd.ODBCCommand(ConnectionString, Commands)
            End If

            Return m_return
        Catch ex As Exception
            Return False
        End Try
    End Function

    ''' <summary>
    ''' OLE DB Command
    ''' </summary>
    ''' <param name="ConnectionString">The connection string</param>
    ''' <param name="Toggle">Toggle the connection on/off</param>
    ''' <param name="Commands">The SQL Command</param>
    ''' <returns>Command Succes or Fail</returns>
    ''' <search>interoperability, database, slingshot, query</search>
    Public Function OLEDB_Command(ConnectionString As String,
                                      Toggle As Boolean,
                                      Commands As List(Of String)) As Boolean

        Try
            Dim m_return As Boolean = False
            If Toggle = True Then
                Dim m_dbcmd As New clsRDBMS()

                m_return = m_dbcmd.OLEDBCommand(ConnectionString, Commands)
            End If

            Return m_return
        Catch ex As Exception
            Return False
        End Try
    End Function

    ''' <summary>
    ''' MySQL Command
    ''' </summary>
    ''' <param name="ConnectionString">The connection string</param>
    ''' <param name="Toggle">Toggle the connection on/off</param>
    ''' <param name="Commands">The SQL Command</param>
    ''' <returns>Command Succes or Fail</returns>
    ''' <search>interoperability, database, slingshot, query</search>
    Public Function MySQL_Command(ConnectionString As String,
                                      Toggle As Boolean,
                                      Commands As List(Of String)) As Boolean

        Try
            Dim m_return As Boolean = False
            If Toggle = True Then
                Dim m_dbcmd As New clsRDBMS()

                m_return = m_dbcmd.MySQLCommand(ConnectionString, Commands)
            End If

            Return m_return
        Catch ex As Exception
            Return False
        End Try

    End Function

    ''' <summary>
    ''' SQLite Command
    ''' </summary>
    ''' <param name="FilePath">The connection string</param>
    ''' <param name="Toggle">Toggle the connection on/off</param>
    ''' <param name="Commands">The SQL Command</param>
    ''' <returns>Command Succes or Fail</returns>
    ''' <search>interoperability, database, slingshot, query</search>
    Public Function SQLite_Command(FilePath As String,
                                      Toggle As Boolean,
                                      Commands As List(Of String)) As Boolean
        Try
            Dim m_return As Boolean = False
            If Toggle = True Then
                Dim m_dbcmd As New clsRDBMS()

                m_return = m_dbcmd.SQLiteCommand(FilePath, Commands)
            End If

            Return m_return
        Catch ex As Exception
            Return False
        End Try
    End Function
#End Region

End Module